# Proyecto IoT-SOA #


## ARDUINO UNO + 7 SEGMENT + bcd to 7 segment decoder ##

![andando1.jpg](https://bitbucket.org/repo/ekk4EqG/images/2561747279-andando1.jpg)

## ARDUINO UNO + WiFi MODULE ESP8266 ##
####ESP8266 pinout####
![esp8266-pinout.jpg](https://bitbucket.org/repo/ekk4EqG/images/729002111-esp8266-pinout.jpg)

####Conexión ARDUINO --> ESP8266####
![esp8266.jpg](https://bitbucket.org/repo/ekk4EqG/images/2755232728-esp8266.jpg)


####NodeMCU flashing tool GitHub####
https://github.com/nodemcu/nodemcu-flasher

#### AT COMMANDS ####
| COMMAND			| DESCRIPTION					| SET/EXECUTE
| :--------:		| :----------:					| :------------: |     
| AT+RST			| restart the module			| – 
| AT+CWMODE			| wifi mode						| AT+CWMODE=<mode>, AT+CWMODE?
| AT+CWJAP			| join the AP					| AT+ CWJAP =<ssid>,< pwd >
| AT+CWLAP			| list the AP					| AT+CWLAP
| AT+CWQAP			| quit the AP					| AT+CWQAP
| AT+ CWSAP			| set the parameters of AP		| AT+ CWSAP= <ssid>,<pwd>,<chl>, <ecn>
| AT+ CIPSTATUS		| get the connection status		| AT+ CIPSTATUS
| AT+CIPSTART		| set up TCP or UDP connection	| (+CIPMUX=0) AT+CIPSTART= <type>,<addr>,<port>
| AT+CIPSEND		| send data						| (+CIPMUX=0) AT+CIPSEND=<length>
| AT+CIPCLOSE		| close TCP or UDP connection	| AT+CIPCLOSE=<id> or AT+CIPCLOSE
| AT+CIFSR			| Get IP address				| AT+CIFSR
| AT+ CIPMUX		| set mutiple connection		| AT+ CIPMUX=<mode>
| AT+ CIPSERVER		| set as server					| AT+ CIPSERVER= <mode>[,<port> ]
| +IPD				| received data					| -
| AT+CIOBAUD		| Change baud rate				| AT+CIOBAUD=9600