int OUT_RED_PIN = 11;
int OUT_BLUE_PIN = 10;
int RANGE = 20;
int TEMP_PORT = 12;
//int crescendo = 2;

int DISPLAY_PORTS[2][4] = {
  {5,4,3,2},
  {6,7,8,9}
};

void setup() {
  Serial.begin(9600);
  pinMode(TEMP_PORT, INPUT);
  pinMode(OUT_RED_PIN, OUTPUT);
  pinMode(OUT_BLUE_PIN, OUTPUT);
  for(int i = 0; i < 2;i++){
    for(int j = 0; j < 4; j++){
      pinMode(DISPLAY_PORTS[i][j], OUTPUT);
    }
  }
}

void loop() {
  //if(crescendo > 50)
  //  crescendo = 2;
int button = 1;
float temp;
if ( button == 0 ){
temp = getTemp("Sensor");}
else{
temp = getTemp("Wifi");
}
  //temp = crescendo;

 // int redLight = calculateRedLight(temp);

 // int blueLight = 255 - redLight;

  //analogWrite(OUT_RED_PIN, redLight);
  //analogWrite(OUT_BLUE_PIN, blueLight);
  showTemperature(temp);
  delay(400);
  //crescendo++;
}

float getTemp( String type ) {
 float temperature;
 if ( type.equals("Sensor")){
    int a = getRoomtemp(TEMP_PORT);
    float resistance=(float)(1023-a)*10000/a;
     temperature=1/(log(resistance/10000)/3975+1/298.15)-273.15;
 }
 else
 {
  //DE INTERNET
  temperature = 10;
 }
 return temperature;
}

int getRoomtemp(int pin) {
    long sum = 0;
    for(int i=0; i<32; i++)
        sum += analogRead(pin);

    return ((sum>>5))*50/33;
}

float calculateRedLight(int temp) {
  int redLight = (255*temp) / 20 - 3060 / 20;
  if(redLight > 255)
    redLight = 255;
  if(redLight < 0)
    redLight = 0;
  return redLight;
}

void showTemperature(float temperature) {
  int temp = temperature;
  int firstDigit = temp / 10;
  int secondDigit = temp % 10;
  setDisplayValue(0, secondDigit);
  setDisplayValue(1, firstDigit);

}

void setDisplayValue(int displayNumber, int value) {
  for(int k = 0; k < 4; k++) {
    bool b = (value & ( 1 << k )) >> k;
    digitalWrite(DISPLAY_PORTS[displayNumber][k], b ? HIGH : LOW);
  }
}
