/*LIBRARIES*/
#include <DHT.h>
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
/*CONSTANTS*/
#define SENSOR 1
#define CLOUD 2
#define NO_CONNECTION -1
#define LINE_ENDING "\r\n"
const String SERVER="192.168.0.3";
const String PATH="/chobi";
/*TEMPERATURE SENSOR*/
#define DHTTYPE DHT11
#define TEMP_PORT 2
DHT dht(TEMP_PORT, DHTTYPE);
/*LEDS*/
#define OUT_RED_PIN 11
#define OUT_BLUE_PIN 10
/*WIFI MODULE*/
#define TX 13
#define RX 12
SoftwareSerial SSoft(TX, RX);

struct Temperature {
  char location[25] = "Buenos Aires";
  float temperature;
  int humidity;
};

struct Arduino {
  char ip[25];
  bool light = 1;
  int option;
  Temperature cloud;
  Temperature sensor;
};

Arduino arduino;

//int DISPLAY_PORTS[2][4] = {
//  {5,4,3,2},
//  {9,8,7,6}
//};

void setup() {
  Serial.begin(9600);
  SSoft.begin(115200);
  dht.begin();
  sendCommand("AT+CIOBAUD=9600\r\n", 30);
  SSoft.begin(9600);
  sendCommand("AT+CWMODE=3\r\n",1000);
  sendCommand("AT+CIPMUX=1\r\n",1000);
  sendCommand("AT+CIPSERVER=1,80\r\n",1000);
  sendCommand("AT+CWJAP=\"#real\",\"Af84005719\"\r\n",10000);
//  sendCommand("AT+CIFSR\r\n", 1000, DEBUG);
  Serial.println("WIFI MODULE STARTED");
  readSensorTemp();
  //sendIP();
  
  pinMode(OUT_RED_PIN, OUTPUT);
  pinMode(OUT_BLUE_PIN, OUTPUT);
  
//  for(int i = 0; i < 2;i++) {
//    for(int j = 0; j < 4; j++) {
//       pinMode(DISPLAY_PORTS[i][j], OUTPUT);
//    }
//  }
}

void loop() {
  Temperature current;
  int connectionID = readRequests();
  if(connectionID != NO_CONNECTION) {
    readSensorTemp();
    sendResponse(connectionID);
  }
  switch(arduino.option) {
      case CLOUD:
        current = arduino.cloud;
        break;
      case SENSOR:
      default:
        current = arduino.sensor;
  }
  
  int redLight = calculateRedLight(current.temperature);
  int blueLight = 255 - redLight;
  if(arduino.light) {
    analogWrite(OUT_RED_PIN, redLight);
    analogWrite(OUT_BLUE_PIN, blueLight);
  } else {
    analogWrite(OUT_RED_PIN, 0);
    analogWrite(OUT_BLUE_PIN, 0);
  }
  
//  showTemperature();
}

//LEER REQUESTS DEL SERVIDOR
int readRequests() {
  if(SSoft.available()) {
    String request = SSoft.readString();
    int requestIndex = request.indexOf("+IPD,");
    if(requestIndex > 0) {
      int optionIndex = request.indexOf("option=") + 7;
      int option = optionIndex > 0 ? request.charAt(optionIndex) - 48 : arduino.option;
      int connectionID = request.charAt(requestIndex + 5) - 48;
      
      request = request.substring(request.indexOf("{"));
      request.replace(": ", ":");
      request.replace("\t", "");
      request.replace("\n", "");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(request);

      if(root.success()) {
        strcpy(arduino.ip, root["ip"]);
        arduino.light = root["light"].as<bool>();
        arduino.option = option;
        strcpy(arduino.cloud.location, root["cloud"]["location"]);
        arduino.cloud.temperature = root["cloud"]["temperature"].as<float>();
        arduino.cloud.humidity = root["cloud"]["humidity"].as<int>();
      }

      return connectionID;
    }
  }
  return NO_CONNECTION;
}

//ENVIAR RESPONSE AL SERVIDOR
void sendResponse(int connectionId) {
  String data = createResponseJSON();

  String response = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n" + data;
  postData(response, connectionId, false);
}

//ENVIAR IP AL SERVIDOR
void sendIP() {
  String data = createResponseJSON();
   
  String postRequest = "POST " + PATH + " HTTP/1.1\r\nHost: " + SERVER + "\r\n" + 
                       "Accept: */*\r\nContent-Length: " + data.length() + "\r\n" +
                       "Content-Type: application/json\r\n\r\n" + data;
  
  postData(postRequest, 0L, true);
}

//CREAR JSON
String createResponseJSON() {
  char data[256];
  StaticJsonBuffer<256> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonObject& sensor = root.createNestedObject("sensor");

  root["light"] = arduino.light ? true : false;
  sensor["temperature"] = arduino.sensor.temperature;
  sensor["humidity"] = arduino.sensor.humidity;
  root.printTo(data, sizeof(data));

  return String(data);
}

//POST DATOS AL SERVIDOR
void postData(String postRequest, const long connectionId, boolean start) {
    //Establecer conexion con el servidor si es la primera vez.
    if(start) {
      String command = "AT+CIPSTART=";command.concat(String(connectionId));command.concat(",\"TCP\",\"");
      command.concat(SERVER);command.concat("\",8080");command.concat(LINE_ENDING);
      sendCommand(command, 500);
    }

    //Se realiza el post al servidor
    String command = "AT+CIPSEND=";command.concat(connectionId);command.concat(',');command.concat(postRequest.length());command.concat(LINE_ENDING);
    SSoft.print(command);

    if(SSoft.find(">")) { 
      SSoft.print(postRequest);
      if(SSoft.find("SEND OK")) { 
        closeConection(connectionId);
      }
    }
}

//CERRAR CONEXION
void closeConection(int connectionId) {
  String command = "AT+CIPCLOSE=";command.concat(String(connectionId));command.concat(LINE_ENDING);
  sendCommand(command, 50);
}

//ENVIAR COMANDOS AL MODULO ESP8266
String sendCommand(String command, const int timeout) {
    String buffer = "";    
    SSoft.print(command);
    long int time = millis();
    while((time + timeout) > millis())
      while(SSoft.available())
        char c = SSoft.read();
   
    return buffer;
}

float calculateRedLight(const float temperature) {
  int redLight = (255 * temperature) / 20 - 3060 / 20;
  if(redLight > 255)
    redLight = 255;
  if(redLight < 0)
    redLight = 0;
  return redLight;
}

void readSensorTemp() {
  arduino.sensor.humidity = dht.readHumidity(); //Se lee la humedad
  arduino.sensor.temperature = dht.readTemperature(); //Se lee la temperatura
}
